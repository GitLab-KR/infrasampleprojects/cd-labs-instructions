## Creating a project called simple-java

In this lab, you will go through steps to create a project called *simple-java*.

### Creating the project

Log on to your GitLab workspace. The first time you log in, you should see the following screen. Click on *Create a project*.

> **NOTE:** if this is not the first time you log in to GitLab workspace, head to your **Projects** window and click on the *New Project* blue button on the top right of your screen.

<img src="images/ado/ado-1-create-project.png" width="50%" height="50%">

On the next screen, click on *Import project*.

<img src="images/ado/ado-2-import-project.png" width="50%" height="50%">

On the next screen, click on *Repo by URL*.

<img src="images/ado/ado-3-import-from.png" width="75%" height="75%">

The screen will expand and show fields to populate for the import of a project via its URL.

<img src="images/ado/ado-4-enter-proj-info.png" width="50%" height="50%">

Enter the following project URL in the field **Git repository URL** field:

> https://gitlab.com/tech-marketing/sandbox/simple-java.git

and ensure that *Public* is checked for **Visibility Level**. Your screen should look similar to the following one:

<img src="images/ado/ado-5-enter-proj-URL-filled-out.png" width="50%" height="50%">

Next, click on the *Create project* button. You will see the following message for a few seconds while the project is being imported:

<img src="images/ado/ado-6-import-in-progress.png" width="50%" height="50%">

After a few seconds, the screen above will clear and you will be put into the newly created project:

<img src="images/ado/ado-7-proj-imported.png" width="50%" height="50%">

### Create a project-level EKS cluster

> **NOTE:** if you have already created a group-level EKS cluster for all your lab projects, then SKIP THIS SECTION!

On the left margin of your project window, click on **Operations > Kubernetes** to go to the Kubernetes screen:

---

<img src="images/auto-deploy/7-K8s-screen.png" width="50%" height="50%">

---

Click on the *Integrate with a cluster certificate* green button to go to the **Add a Kubernetes cluster integration** screen:

---

<img src="images/auto-deploy/8-click-on-create-EKS.png" width="75%" height="75%">

---

On this screen you can see tiles for GitLab's integrated cluster creation options. Since this lab is about creating an EKS cluster, click on the **Amazon EKS** tile to expand the screen with the input fields needed by the integration for the creation of an EKS cluster:

---

<img src="images/auto-deploy/9-enter-your-role-ARN.png" width="75%" height="75%">

---

In the above screen, you will see the default **Account ID** and **External ID**, which are readonly. You need to enter your **Provision Role ARN** (the string *arn:aws:iam::[AWS ACCOUNT]:role/[IAM USER]* is just an example of its syntax and not a real Provision Role ARN). Besides the **Provision Role ARN**, you will also need a **Service role**, **Key pair name**, and **VPC** with its subnets and security group. To create all these, follow these [instructions](subchapters/create-aws-roles-for-eks.md).

If you're back at this point, it means that you have created all the AWS resources needed by the GitLab integrated EKS cluster creation. So, enter your provision role ARN in the field **Provision Role ARN**. For the **Cluster region** field, enter the same region you used for the creation of the required cluster **VPC**, or you can leave it blank if you created your **VPC** in the us-east-2 (Ohio) region, which is the default for this field.

Click on the *Authenticate with AWS* button at the bottom of the screen. This will take you to the **Enter the details for your Amazon EKS Kubernetes cluster** window. On this window, enter the following values:

| Field | Value | Notes
| ----------- | ----------- | ----------- |
| Kubernetes cluster name | [YOUR GITLAB ID]-simple-java-eks-cluster | you can name it whatever you'd like but we recommend adding your GitLab handle in front so that you can easily find your cluster during a search. Replace *[YOUR GITLAB ID]* with your GitLab handle
| Environment scope | * | indicates that this cluster will be used for all environments you create within this project
| Kubernetes version | leave its default value
| Service role | eksClusterRole | if you followed the instructions, this should be the name for the service role
| Cluster Region | readonly and cannot change
| Key pair name | enter the one you created | if you followed the instructions, this should be *[YOUR GITLAB ID]-key-pair*
| VPC | enter the one you created | click on the field to open the search and type in the name of the VPC you created. If you followed the instructions, this should be *[YOUR GITLAB ID]-eks-vpc-VPC*
| Subnets | the three subnets you created | click on the field to open the search and the list of all your subnets. Select the ones you created one at a time and ensure that all three are entered in this field
| Security group | the name of the security group you created | click on the field to open the search and the list of all your security groups. If you followed the instructions, you should see a security group name *[YOUR GITLAB ID]-eks-vpc-ControlPlaneSecurityGroup-[long alphanumeric string]* in the list. Select this one. If you're not sure which security group, go to your **AWS Console > VPC Dashboard > Security > Security Groups** and locate the **Security group ID** for the security group you created. Its name will be in the column **Security group name**
| Instance type | m5.large | 2 vCPUs and 8GB RAM
| Number of nodes | 3 | good number for 1 master node and 2 worker nodes
| GitLab-managed cluster | checked | we want GitLab to manage this cluster
| Namespace per environment | checked | indicates that every environment will use its own namespace

Once you have populated all the fields in the **Enter the details for your Amazon EKS Kubernetes cluster** window, scroll to the bottom of the screen and click on the *Create Kubernetes Cluster* green button:

---

<img src="images/auto-deploy/38-click-on-create-cluster.png" width="75%" height="75%">

---

You will be put in the **Project cluster** window and you will see the following message while the EKS cluster is being created:

---

<img src="images/auto-deploy/39-cluster-being-created.png" width="30%" height="30%">

---

After 10-15 minutes, the creation process will finish and your newly created cluster window will be displayed:

---

<img src="images/auto-deploy/40-eks-cluster-details.png" width="75%" height="75%">

---

Click on the *Applications* tab to see of the applications that you can deploy to the running EKS cluster with a click of a button:

---

<img src="images/auto-deploy/41-click-on-app-and-install.png" width="75%" height="75%">

---

Click on the *Install* buttons for the *Ingress*, *Cert-Manager* and *Prometheus* applications to get them installed on your newly created cluster. Wait until all three applications are installed. You will know that they are all installed when their *Install* button are re-labeled *Uninstall*.

Copy and save the value for the *Ingress Endpoint*, which was populated when the *Ingress* application was successfully installed. Here's a screenshot of the *Ingress Enpoint* that I got (yours will be different):

---

<img src="images/auto-deploy/42-copy-ingress-endpoint.png" width="75%" height="75%">

---

You now need to create a DNS CNAME (alias) record for this ingress endpoint with a public DNS service. I used Google Cloud DNS but you can use any other one, e.g. AWS Route53. You need to create two CNAME records, one for the domain and one for the wildcard domain. I'm using the domain **tmm.tanuki.host**, which I already own (the domain that you own will be different). Here's a screenshot of the domain CNAME record creation:

---

<img src="images/auto-deploy/43-create-DNS-entry.png" width="40%" height="40%">

---

and here's a screenshot of the wildcard domain CNAME record creation:

---

<img src="images/auto-deploy/44-create-wildcard-DNS-entry.png" width="40%" height="40%">

----
**NOTE:** You can find more examples of how to set DNS CNAME (alias) records with different cloud DNS services [here](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/examples-for-setting-DNS-CNAME-record.md).

The last thing you need to do is to set your newly create EKS cluster base domain. Back on the EKS cluster detail window, click on the *Details* tab:

---

<img src="images/auto-deploy/45-set-base-domain.png" width="50%" height="50%">

---

In the *Base domain* field above, paste the public domain you used in your CNAME record. In my case, it was *tmm.tanuki.host*:

That's it! The EKS cluster is not all set to be used for deployments in your project.

Click [here](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/01.%20Auto%20DevOps.md#creating-a-project-and-enabling-auto-devops) to continue with this lab.
