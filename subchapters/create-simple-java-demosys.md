## Creating a project called simple-java
In this lab, you will go through steps to create a project called *simple-java*.

1. **Creating a project**

Log on to your assigned GitLab Demo System workspace and head over to your demo group by following these [instructions](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/open-your-demo-group.md) (when done, click the *Back* button on your browser to return to this page).

From your demo group:

<img src="images/demo-system/6-your-group.png" width="50%" height="50%">

Click on the *New project* green button on the top right. This will take you to the *Create new project* screen:

<img src="images/demo-system/7-click-on-create-from-template.png" width="50%" height="50%">

Click on the *Create from template* tile and select the *Group* tab on the **Create from template** screen:

<img src="images/demo-system/8-template-group-tab.png" width="50%" height="50%">

From the list of templates, click on the *Use template* green button for the project template named **simple-java**:

<img src="images/demo-system/9-enter-simple-java.png" width="50%" height="50%">

Enter *simple-java* in the **Project name** field above. Then, click on the *Create project* green button at the bottom of the screen. You will temporarily see the following screen:

<img src="images/demo-system/10-proj-creation-in-progress.png" width="50%" height="50%">

After a few seconds, the screen above will clear and you will be put into the newly created project:

<img src="images/demo-system/15-simple-java-proj-created.png" width="50%" height="50%">

Click [here](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/01.%20Auto%20DevOps.md#creating-a-project-and-enabling-auto-devops) to continue with this lab.
