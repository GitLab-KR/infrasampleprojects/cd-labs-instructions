# Auto DevOps Minimum Requirements (reading lab)

GitLab offerings include SaaS (gitlab.com) and self-managed. What are the minimum requirements to use Auto DevOps on these offerings? Let's go over these next.

## Minimum Requirements

You can set up Auto DevOps for Kubernetes, Amazon Elastic Container Service (ECS), or Amazon Cloud Compute. Check out the [Auto DevOps requirements page](https://docs.gitlab.com/ee/topics/autodevops/requirements.html) for further information.

### Auto DevOps requirements for Kubernetes

These are the minimum requirements you need to run Auto DevOps on K8s:

- Kubernetes (for Auto Review Apps, Auto Deploy, and Auto Monitoring).
  - A Kubernetes 1.12+ cluster for your project.
  - NGINX Ingress.
- Base domain (for Auto Review Apps, Auto Deploy, and Auto Monitoring). This domain must be configured with wildcard DNS.
- GitLab Runner (for all stages).
- Prometheus (for Auto Monitoring).
- cert-manager (optional, for TLS/HTTPS).

If you don’t have Kubernetes or Prometheus installed, then Auto Review Apps, Auto Deploy, and Auto Monitoring are skipped.

### Auto DevOps requirements for Amazon ECS

These are the minimum requirements you need to run Auto DevOps on ECS:

- A valid Task Definition for your application. You do this on AWS.
- A running ECS cluster. You do this on AWS.
- An Application Load Balancer to front the ECS cluster.
- A Cluster Service, which could have either a Fargate or ECS launch type.
- Project variable *AUTO_DEVOPS_PLATFORM_TARGET* defined as either *FARGATE* or *ECS*.

> **NOTE:** GitLab Managed Apps are not available when deploying to AWS ECS. You must manually configure your application (such as Ingress or Help) on AWS ECS.

> **NOTE:** If you have both a valid AUTO_DEVOPS_PLATFORM_TARGET variable and a Kubernetes cluster tied to your project, only the deployment to Kubernetes runs.

### Auto DevOps requirements for Amazon EC2

These are the minimum requirements you need to run Auto DevOps on EC2 (Check out [custom build job for Auto DevOps](https://docs.gitlab.com/ee/ci/cloud_deployment/index.html#custom-build-job-for-auto-devops) for detail information):

- Create a CloudFormation stack file in JSON and upload it to your project. You need to follow the AWS instructions on how to do this.
- Create a deployment file in JSON and upload it to your project.  You need to follow the AWS instructions on how to do this.
- Create a JSON file to push your application to S3 and upload it to your project. GitLab provides a template for this.
- Create the following three project variables CI_AWS_CF_CREATE_STACK_FILE, CI_AWS_S3_PUSH_FILE, CI_AWS_EC2_DEPLOYMENT_FILE to point to the respective JSON files.

## On GitLab.com

Here are some key points to know about Auto DevOps on GitLab.com:

- Auto DevOps is available on all tiers of GitLab.com. 
- Auto DevOps is not enabled by default on GitLab.com.
- GitLab.com users can enable or disable Auto DevOps at the project or group level.
- If you use GitLab.com, GitLab manages runners for you. These runners are enabled for all projects, though you can disable them. If you don’t want to use runners managed by GitLab, you can install GitLab Runner and register your own runners on GitLab.com.

## On Self-managed instances

- On self-managed instances, Auto DevOps is enabled by default for all projects. It can be enabled/disabled by instance admin.
- If you use a self-managed instance of GitLab, you must configure the Google OAuth2 OmniAuth Provider before configuring a cluster on GKE.
- Self-managed users can enable or disable Auto DevOps at the project, group, or instance level.
- You can choose to install the GitLab Runner application on infrastructure that you own or manage. If you do, you should install GitLab Runner on a machine that’s separate from the one that hosts the GitLab instance.

## Common characteristics

Here are some characteristics of Auto DevOps common across all offerings:

- Since GitLab 12.7, Auto DevOps runs on pipelines automatically only if a Dockerfile or matching buildpack exists.
- If a CI/CD configuration file is present in the project, it continues to be used, whether or not Auto DevOps is enabled.
- In GitLab 13.0 and later, it is possible to leverage Auto DevOps to deploy to AWS ECS.
- In GitLab 13.6 and later, it is possible to leverage Auto DevOps to deploy to AWS EC2.

## Reference Architectures for self-managed GitLab installations

We provide a series of reference architectures to accomodate GitLab installations to handle different user loads:

- Up to 1,000 users
- Up to 2,000 users
- Up to 3,000 users
- Up to 5,000 users
- Up to 10,000 users
- Up to 25,000 users
- Up to 50,000 users

> **NOTE:** A GitLab Premium or Ultimate license is required to get assistance from Support with troubleshooting the 2,000 users and higher reference architectures.

For further information, check out the [reference archictures page](https://docs.gitlab.com/ee/administration/reference_architectures/).
